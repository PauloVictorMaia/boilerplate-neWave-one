import React from 'react';
import PropTypes from 'prop-types';
import {
  LabelForm,
  InputForm,
  InputContainer,
  MaskedInput,
} from './input.style';

const Input = ({
  placeholder,
  label,
  type,
  setValue,
  id,
  masked,
  ...props
}) => {
  return (
    <InputContainer>
      <LabelForm htmlFor={id}>{label}</LabelForm>
      {masked ? (
        <MaskedInput {...props} />
      ) : (
        <InputForm
          id={id}
          name={id}
          placeholder={placeholder}
          type={type}
          onChange={({ target }) => setValue(target.value)}
        />
      )}
    </InputContainer>
  );
};

Input.propTypes = {
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  masked: PropTypes.bool,
  setValue: PropTypes.any,
  placeholder: PropTypes.string,
};

export default Input;
