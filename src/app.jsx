import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Switch, Route } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import GlobalStyle from 'styles/globalStyles';
import ROUTES from 'constants/routes';

const history = createHistory({
  basename: '',
  forceRefresh: false,
});

const render = () => {
  ReactDOM.render(
    <>
      <GlobalStyle />
      <Router history={history}>
        {ROUTES && (
          <Switch>
            {ROUTES.map((item, i) => {
              const { exact, component, path } = item;
              const AsyncComponent = require(`pages/${component}.jsx`).default;
              return (
                <Route
                  key={component}
                  path={path}
                  exact={exact}
                  component={() => <AsyncComponent />}
                />
              );
            })}
          </Switch>
        )}
      </Router>
    </>,
    document.getElementById('root'),
  );
};

render();
