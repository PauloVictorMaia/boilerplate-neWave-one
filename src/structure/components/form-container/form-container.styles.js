import styled from 'styled-components';
import { rem, mediaQuery } from 'styles/variables';
import 'styles/fonts';

export const Form = styled.form`
  align-items: center;
  display: flex;
  flex-direction: column;
  height: 80%;
  justify-content: space-around;
  padding: ${rem(10)};

  ${mediaQuery.laptop`
    width: 50%;
  `}
`;
