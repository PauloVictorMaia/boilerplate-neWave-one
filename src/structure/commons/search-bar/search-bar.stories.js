import React from 'react';
import SearchBar from '.';
import GlobalStyles from 'styles/globalStyles';
import { withKnobs } from '@storybook/addon-knobs';

export default {
  title: 'SearchBar',
  decorators: [withKnobs],
  parameters: {
    backgrounds: [{ name: 'black', value: '#000' }, { name: 'white', value: '#fff' }],
  },
};

export const Default = () => (
  <>
    <GlobalStyles />
    <SearchBar />
  </>
);
