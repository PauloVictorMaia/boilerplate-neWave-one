import React from 'react';
import PropTypes from 'prop-types';

import { Container } from './icon-container.style';

const IconContainer = ({ color, height, icon, width }) => {
  const Icon = icon;

  return (
    <Container color={color} height={height} width={width}>
      <Icon />
    </Container>
  );
};

IconContainer.propTypes = {
  color: PropTypes.string,
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.oneOf(['auto'])])
    .isRequired,
  icon: PropTypes.func.isRequired,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.oneOf(['auto'])]),
};

IconContainer.defaultProps = {
  color: '#000000',
  height: 'auto',
  icon: () => {},
  width: 'auto',
};

export default IconContainer;
