import styled from 'styled-components';
import { rem, mediaQuery, colors } from 'styles/variables';

export const LoginTitle = styled.h3`
  color: ${colors.textColor};
  font-size: ${rem(18)};
  font-weight: 700;
`;

export const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;

  ${mediaQuery.laptop`
   width: 50%;
 `}
`;

export const ForgotSpan = styled.span`
  font-size: ${rem(10)};
  color: ${colors.spanColor};
  cursor: pointer;
`;
