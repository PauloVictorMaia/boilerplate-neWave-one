import styled from 'styled-components';
import { colors, rem } from 'styles/variables';

export const LabelForm = styled.label`
  font-size: ${rem(13)};
  margin-bottom: ${rem(5)};
  color: ${colors.grey};
  padding: ${rem(3)};
`;

export const SelectContainer = styled.div`
  width: 100%;
`;

export const SelectForm = styled.select`
  background: none;
  border: ${rem(1)} solid ${colors.grey};
  border-radius: ${rem(4)};
  display: block;
  height: ${rem(35)};
  margin-bottom: ${rem(15)};
  max-width: 100%;
  padding-left: ${rem(9)};
  width: 100%;
`;

export const SelectOption = styled.option`
  color: ${colors.black};
`;
