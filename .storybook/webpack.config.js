const webpack = require('webpack');
const { resolve } = require('path');

const commonConfig = require('./../config/config.json');
const aliasConfig = require('../config/alias');

module.exports = {
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.(png|jpe?g|gif|ico|mp4|mov|svg|webm|pdf|zip)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name].[ext]',
              useRelativePath: false,
              outputPath: 'assets',
              context: 'assets',
            },
          },
        ],
      },
    ],
  },
  resolve: {
    modules: [resolve(__dirname, '../src'), 'node_modules'],
    extensions: ['.js'],
    alias: aliasConfig,
  },
  plugins: [
    new webpack.DefinePlugin({
      ns: JSON.stringify(commonConfig.namespace),
    }),
  ],
};
