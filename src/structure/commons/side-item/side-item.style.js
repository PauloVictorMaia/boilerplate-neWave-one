import styled from 'styled-components';
import fonts from 'styles/fonts';

import { rem } from 'styles/variables';

export const Container = styled.div`
  align-items: center;
  cursor: pointer;
  display: flex;
`;

export const Text = styled.p`
  ${fonts.small()}
  color: ${({ active }) => (active ? 'black' : '#505050')};
  font-weight: ${({ active }) => (active ? 700 : 'normal')};
  margin-left: ${rem(10)};
`;
