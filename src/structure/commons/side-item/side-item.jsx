import React from 'react';
import PropTypes from 'prop-types';

import IconContainer from 'commons/icon-container';

import { Container, Text } from './side-item.style';

const SideItem = ({ active, icon, text }) => {
  return (
    <Container>
      <IconContainer color={active ? 'black' : '#505050'} height={20} icon={icon} width={20} />
      <Text active={active}>{text}</Text>
    </Container>
  );
};

SideItem.propTypes = {
  active: PropTypes.bool.isRequired,
  icon: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
};

export default SideItem;
