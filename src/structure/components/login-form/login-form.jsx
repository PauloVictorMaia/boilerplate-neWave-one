import React, { useState } from 'react';
import Input from 'commons/input';
import Button from 'commons/button';
import FormContainer from 'components/form-container';
import { LoginTitle, InputContainer, ForgotSpan } from './login-form.style';

const LoginForm = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  return (
    <FormContainer>
      <LoginTitle>Log in</LoginTitle>
      <InputContainer>
        <Input
          id="email"
          name="email"
          label="Insira seu email"
          type="text"
          value={email}
          setValue={setEmail}
        />

        <Input
          id="password"
          name="password"
          label="Insira sua senha"
          type="password"
          value={password}
          setValue={setPassword}
        />
      </InputContainer>
      <Button text="Entrar" />
      <ForgotSpan>Esqueci minha senha</ForgotSpan>
    </FormContainer>
  );
};

export default LoginForm;
