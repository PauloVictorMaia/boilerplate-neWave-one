const { resolve } = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const commonConfig = require('./config.json');

module.exports = {
  entry: ['@babel/polyfill', 'react-hot-loader/patch', resolve(__dirname, '../src/app.jsx')],
  devtool: 'inline-source-map',
  plugins: [
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false,
      options: {
        context: __dirname,
      },
    }),
    new HtmlWebpackPlugin({
      title: commonConfig.title,
      namespace: commonConfig.namespace,
      template: '../src/index-dev.html',
      filename: 'index.html',
    }),
  ],
};
