import React from 'react';
import PropTypes from 'prop-types';
import {
  TitleSelector,
  SelectorContainer,
  SelectorContent,
  SelectorTop,
  SelectorBottom,
} from './selector.style';
import ItemSelector from 'commons/item-selector';

const Selector = ({ title, items }) => {
  return (
    <>
      <TitleSelector>{title}</TitleSelector>
      <SelectorContainer>
        <SelectorTop />
        <SelectorContent>
          {items &&
            items.map((item, i) => <ItemSelector key={i} name={item} />)}
        </SelectorContent>
        <SelectorBottom />
      </SelectorContainer>
    </>
  );
};

Selector.propTypes = {
  title: PropTypes.string,
  items: PropTypes.array.isRequired,
};

export default Selector;
