const { resolve } = require('path');

module.exports = {
  styles: resolve(__dirname, '../src/styles'),
  config: resolve(__dirname, '../src/config'),
  structure: resolve(__dirname, '../src/structure'),
  pages: resolve(__dirname, '../src/pages'),
  constants: resolve(__dirname, '../src/constants'),
  components: resolve(__dirname, '../src/structure/components'),
  icons: resolve(__dirname, '../src/structure/icons'),
  commons: resolve(__dirname, '../src/structure/commons'),
  containers: resolve(__dirname, '../src/structure/containers'),
  data: resolve(__dirname, '../src/data'),
  assets: resolve(__dirname, '../assets'),
};
