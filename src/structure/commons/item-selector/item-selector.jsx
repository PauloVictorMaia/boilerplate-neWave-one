import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  SelectorItem,
  SelectorText,
  SelectorIcon,
} from './item-selector.style';

const ItemSelector = ({ name }) => {
  const [active, setActive] = useState(false);

  return (
    <SelectorItem onClick={() => setActive(!active)} active={active}>
      <SelectorIcon active={active} />
      <SelectorText>{name}</SelectorText>
    </SelectorItem>
  );
};

ItemSelector.propTypes = {
  name: PropTypes.string,
};

export default ItemSelector;
