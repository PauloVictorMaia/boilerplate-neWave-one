import styled from 'styled-components';
import { mediaQuery, rem } from 'styles/variables';

export const InfoHolder = styled.div`
  width: 50%;

  &:first-child {
    margin-right: ${rem(24)};
  }
`;

export const LastInfoComponentContainer = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;

  ${mediaQuery.tablet`
  flex-direction: row;
  justify-content: space-between;
 `};
`;
