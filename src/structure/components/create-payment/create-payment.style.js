import styled from 'styled-components';
import { rem, mediaQuery, colors } from 'styles/variables';

export const AppointmentsContainer = styled.div`
  background-color: ${colors.primaryColor};
  border-radius: ${rem(10)};
  height: 100%;
  margin-bottom: ${rem(10)};
  margin-top: ${rem(10)};
  padding: ${rem(20)};
  width: 100%;

  ${mediaQuery.tablet`
   height: 100%;
 `}
`;

export const FormAppointment = styled.form`
  display: grid;
  grid-template-columns: 1fr;
  margin-left: ${rem(10)};

  ${mediaQuery.tablet`
   grid-template-columns: 1fr ;
   margin-left: 0;
   margin-top: ${rem(30)};
   width: 90%;
 `}
`;

export const ContentSelects = styled.div`
  display: flex;
  flex-direction: column;

  ${mediaQuery.tablet`
  align-items: center;
  flex-direction: row;
   justify-content: space-between;
 `}
`;

export const InputContainer = styled.div`
  width: 100%;

  ${mediaQuery.tablet`
   width: 30%;
 `}
`;

export const TitleForm = styled.h4`
  align-items: center;
  display: flex;
  font-size: ${rem(15)};
  justify-content: space-between;
  margin: ${rem(10)} 0;
`;

export const ButtonsContainer = styled.div`
  display: flex;
`;

export const ConfirmButton = styled.button`
  background-color: ${colors.spanColor};
  border-radius: ${rem(4)};
  color: ${colors.primaryColor};
  height: ${rem(35)};
  width: 50%;

  ${mediaQuery.tablet`
   width: ${rem(130)};
 `}
`;

export const CancelButton = styled.button`
  background-color: ${colors.primaryColor};
  border: ${rem(1)} solid ${colors.gray};
  border-radius: ${rem(4)};
  color: ${colors.gray};
  height: ${rem(35)};
  margin-left: ${rem(10)};
  width: 50%;

  ${mediaQuery.tablet`
   width: ${rem(130)};
 `}
`;
