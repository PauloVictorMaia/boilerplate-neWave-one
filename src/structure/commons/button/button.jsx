import React from 'react';
import PropTypes from 'prop-types';
import { colors } from 'styles/variables';

import { ButtonWrapper } from './button.style';

const Button = ({ text, modifier, disabled, onClick, bgColor }) => {
  return (
    <ButtonWrapper
      bgColor={bgColor}
      modifier={modifier}
      disabled={disabled}
      onClick={onClick}
    >
      {text}
    </ButtonWrapper>
  );
};

Button.propTypes = {
  disabled: PropTypes.bool,
  bgColor: PropTypes.oneOf(Object.keys(colors)),
  modifier: PropTypes.oneOf(['secondary']),
  onClick: PropTypes.func,
  text: PropTypes.string.isRequired,
};

Button.defaultProps = {
  disabled: false,
  modifier: undefined,
  bgColor: undefined,
  onClick: PropTypes.func,
  text: PropTypes.string.isRequired,
};

export default Button;
