const ROUTES = [
  {
    path: '/',
    component: 'login/login',
    exact: true,
  },
  {
    path: '/login',
    component: 'login/login',
    exact: true,
  },
  {
    path: '/dashboard',
    component: 'dashboard/dashboard',
    exact: true,
  },
];

export default ROUTES;
