import Master from 'assets/images/Mastercard.svg';
import Amex from 'assets/images/amex.svg';
import Unimed from 'assets/images/Unimed-logo 1.svg';
import GroupBar from 'assets/images/groupe-bar.svg';

export default [
  {
    image: Master,
    institution: 'Master',
    type: 'cartão de crédito',
    paymentType: 'Parcelado em até 12x',
    state: 'ativo'
  },
  {
    image: Amex,
    institution: 'Amex',
    type: 'cartão de crédito',
    paymentType: 'Parcelado em até 12x',
    state: 'ativo'
  },
  {
    image: Unimed,
    institution: 'Unimed',
    type: 'cartão de crédito',
    paymentType: 'Á vista',
    state: 'ativo'
  },
  {
    image: GroupBar,
    institution: 'Boleto',
    type: 'cartão de crédito',
    paymentType: 'Parcelado em até 12x',
    state: 'ativo'
  },
];