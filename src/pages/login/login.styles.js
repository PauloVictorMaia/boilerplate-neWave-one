import styled from 'styled-components';
import { colors, rem, mediaQuery } from 'styles/variables';
import 'styles/fonts';

export const LoginContainer = styled.div`
 width: 100vw;
 height: 100vh;
 background-color: ${colors.primaryColor};
 display: flex;
 justify-content: center;
 align-items: center;
 padding: ${rem(15)};

 ${mediaQuery.laptop`
   flex-direction: row;
   justify-content: space-around;
 `}
`;

export const LogoImageContainer = styled.div`
 display: none;

 ${mediaQuery.laptop`
   display: block;
   width: 50%;
   height: 100%;
  `}
`;

export const LogoArea = styled.div`
 width: ${rem(400)};
 height: ${rem(400)};
 background-color: ${colors.secondaryColor};
 border-radius: 50%;
`;

