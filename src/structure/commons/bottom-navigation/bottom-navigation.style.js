import styled from 'styled-components';
import { colors, rem, mediaQuery } from 'styles/variables';

export const BottomNavigationContainer = styled.div`
  background-color: ${colors.white};
  max-width: 100%;
  width: 100%;

  ${mediaQuery.tablet`
    display: none;
  `}
`;

export const NavigationContainer = styled.nav`
  height: 100%;
  max-width: 100%;
  width: 100%;
`;

export const ListContainer = styled.ul`
  align-items: center;
  display: flex;
  height: 100%;
  justify-content: space-between;
  max-width: 100%;
  padding: ${rem(16)} ${rem(16)} ${rem(12)};
  width: 100%;
`;
