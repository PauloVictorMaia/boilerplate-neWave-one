import React from 'react';
import LastInfosContent from 'components/last-infos-content/last-infos-content';
import {
  InfoHolder,
  LastInfoComponentContainer,
} from './last-infos-component.style';

const LastInfosComponent = () => {
  return (
    <LastInfoComponentContainer>
      <InfoHolder>
        <LastInfosContent />
      </InfoHolder>
      <InfoHolder>
        <LastInfosContent />
      </InfoHolder>
    </LastInfoComponentContainer>
  );
};

export default LastInfosComponent;
