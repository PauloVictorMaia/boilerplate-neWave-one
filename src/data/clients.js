import Profile from 'assets/images/profile-header-ph.png';

export default [
  {
    image: Profile,
    name: 'Ana Exemplo',
    email: 'ana@exemplo.com.br',
    phone: '(51) 99988-6633',
    locality: 'São Paulo - SP',
  },
  {
    image: Profile,
    name: 'Matheus Henrique',
    email: 'matheus@exemplo.com.br',
    phone: '(11) 99988-6633',
    locality: 'Porto Alegre - RS',
  },
  {
    image: Profile,
    name: 'Guilherme Leonardo',
    email: 'guilherme@exemplo.com.br',
    phone: '(21) 99988-6633',
    locality: 'São Jose dos Campos - SP',
  },
  {
    image: Profile,
    name: 'Rafael Almeida',
    email: 'rafael@exemplo.com.br',
    phone: '(21) 99988-6633',
    locality: 'São Paulo - SP',
  },
  {
    image: Profile,
    name: 'Isabela Molinari',
    email: 'isabela@exemplo.com.br',
    phone: '(21) 99988-6633',
    locality: 'São Paulo - SP',
  },
  {
    image: Profile,
    name: 'Samuel Souza',
    email: 'samuel@exemplo.com.br',
    phone: '(11) 99652-3659',
    locality: 'Salvador - BA',
  },
];
