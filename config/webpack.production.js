const { resolve } = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');

const commonConfig = require('./config.json');

module.exports = {
  entry: ['@babel/polyfill', resolve(__dirname, '../src/app.jsx')],
  devtool: false,
  optimization: {
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          safari10: true,
        },
      }),
    ],
  },
  plugins: [
    new CleanWebpackPlugin(['public'], {
      root: resolve(__dirname, '../'),
      allowExternal: true,
    }),
    new HtmlWebpackPlugin({
      title: commonConfig.title,
      description: commonConfig.description,
      keywords: commonConfig.keywords,
      og: commonConfig.og,
      namespace: commonConfig.namespace,
      template: '../src/index.html',
      filename: 'index.html',
      inlineSource: '.(js)$',
    }),
    new HtmlWebpackInlineSourcePlugin(),
  ],
};
