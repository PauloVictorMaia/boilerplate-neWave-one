import React from 'react';
import {
  InfoCardContainer,
  TitleCard,
  ImageContainer,
  SpanContent,
} from './info-container.style';
import PropTypes from 'prop-types';
import IconContainer from 'commons/icon-container/icon-container';

const InfoCard = ({ image, title, status }) => {
  return (
    <InfoCardContainer>
      <ImageContainer>
        <IconContainer icon={image} width={30} height={30} />
      </ImageContainer>
      <TitleCard>{title}</TitleCard>
      <SpanContent>{status}</SpanContent>
    </InfoCardContainer>
  );
};

InfoCard.propTypes = {
  image: PropTypes.any,
  title: PropTypes.string.isRequired,
  status: PropTypes.string.isRequired,
};

export default InfoCard;
