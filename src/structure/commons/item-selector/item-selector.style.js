import styled from 'styled-components';
import { rem, colors } from 'styles/variables';
import fonts from 'styles/fonts';

export const SelectorIcon = styled.div`
  background-color: ${({ active }) =>
    active ? colors.black : colors.transparent};
  border: ${rem(1)} solid ${({ active }) => (active ? colors.black : colors.grey)};
  border-radius: 100%;
  height: ${rem(9)};
  margin-right: ${rem(7)};
  width: ${rem(9)};
`;

export const SelectorItem = styled.div`
  ${fonts.small()}
  align-items: center;
  border: ${rem(1)} solid
    ${({ active }) => (active ? colors.black : colors.greyDark)};
  border-radius: ${rem(5)};
  cursor: pointer;
  display: flex;
  font-weight: ${({ active }) => (active ? '700' : 'normal')};
  height: ${rem(30)};
  margin: ${rem(8)} ${rem(3)};
  padding: 0 ${rem(16)} 0 ${rem(8)};

  * {
    pointer-events: none;
  }
`;

export const SelectorText = styled.p`
  display: block;
`;
