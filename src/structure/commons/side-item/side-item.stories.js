import React from 'react';
import SideItem from '.';
import { withKnobs } from '@storybook/addon-knobs';

import DashboardIcon from 'icons/Dashboard'

export default {
  title: 'SideItem',
  decorators: [withKnobs],
  parameters: {
    backgrounds: [{ name: 'black', value: '#000' }, { name: 'white', value: '#fff' }],
  },
};

export const Default = () => <SideItem active={false} icon={DashboardIcon} text={'Dashboard'} />;

export const Active = () => <SideItem active={true} icon={DashboardIcon} text={'Dashboard'} />;
