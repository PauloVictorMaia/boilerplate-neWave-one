import React from 'react';

import SearchBar from 'commons/search-bar';

import Profile from 'assets/images/profile-header-ph.png'

import {
  HeaderContainer,
  IconArrow,
  IconConfig,
  IconHamburguer,
  IconNotification,
  ProfileContainer,
  ProfileImage,
  ProfileName,
  ProfilePosition,
  ProfileText,
  SearchContainer,
} from './header.style';

const Header = () => {
  return (
    <HeaderContainer>
      <IconHamburguer />
      <SearchContainer>
        <SearchBar placeholder={'Buscar'} />
      </SearchContainer>
      <IconConfig />
      <IconNotification />
      <ProfileContainer>
      <ProfileImage src={Profile} alt="Profile image"/>
        <ProfileText>
          <ProfileName>Nome Bem Comprido</ProfileName>
          <ProfilePosition>admin</ProfilePosition>
        </ProfileText>
      </ProfileContainer>
      <IconArrow />
    </HeaderContainer>
  );
};

export default Header;
