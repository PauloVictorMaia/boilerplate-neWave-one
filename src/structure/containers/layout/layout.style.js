import styled from 'styled-components';

import { colors, rem, mediaQuery } from 'styles/variables';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 100vh;

  ${mediaQuery.laptop`
      display: block;
    `}
`;

export const Content = styled.div`
  background-color: ${colors.greyDark};
  display: flex;
  flex-direction: column;
  padding: ${rem(16)} ${rem(17)} 0;
  flex-grow: 1;

  ${mediaQuery.laptop`
    height: calc(100vh - ${rem(80)});
    overflow-y: auto;
    padding: ${rem(24)} ${rem(18)} 0 ${rem(32)};
  `}
`;

export const Main = styled.div`
  flex-grow: 1;
  display: flex;
  flex-direction: column;

  ${mediaQuery.laptop`
    flex-direction: row;
  `}
`;
