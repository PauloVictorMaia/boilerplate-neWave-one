import React from 'react';
import PropTypes from 'prop-types';

import IconContainer from 'commons/icon-container';

import Arrow from 'icons/Arrow'
import CalendarIcon from 'icons/Calendar'
import CirclePlusIcon from 'icons/CirclePlus'
import DownloadIcon from 'icons/Download'

import {
  Container,
  Holder,
  HolderItem,
  Text,
  Title,
} from './overview.style';

const Overview = ({ title, filter, add, download }) => {
  return (
    <Container>
      <Title>
        {title}
      </Title>
      <Holder>
        {filter && (
          <HolderItem onClick={filter.onClick}>
            <Text>{filter.text}</Text>
            <IconContainer height={20} icon={Arrow} width={8} />
          </HolderItem>
        )}
        {add && (
          <HolderItem onClick={add.onClick}>
            <Text>{add.text}</Text>
            <IconContainer height={20} icon={CirclePlusIcon} width={16} />
          </HolderItem>
        )}
        {download && (
          <HolderItem onClick={download.onClick}>
            <Text>{download.text}</Text>
            <IconContainer height={20} icon={DownloadIcon} width={14} />
          </HolderItem>
        )}
        <HolderItem>
          <IconContainer height={20} icon={CalendarIcon} width={20} />
        </HolderItem>
      </Holder>
    </Container>
  );
};

Overview.propTypes = {
  add: PropTypes.object,
  download: PropTypes.object,
  filter: PropTypes.object,
  title: PropTypes.string
};

export default Overview;
