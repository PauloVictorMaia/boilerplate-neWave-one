import styled from 'styled-components';
import fonts from 'styles/fonts';

import { colors, rem, mediaQuery } from 'styles/variables';

export const Container = styled.div`
  align-items: center;
  background-color: ${colors.white};
  border-radius: ${rem(5)};
  display: flex;
  height: ${rem(40)};
  justify-content: space-between;
  min-height: ${rem(40)};
  padding: 0 ${rem(10)} 0 ${rem(15)};
  width: 100%;

  ${mediaQuery.laptop`
    height: ${rem(50)};
    min-height: ${rem(50)};
    padding: 0 ${rem(24)} 0 ${rem(16)};
  `}
`;

export const Holder = styled.div`
  display: flex;
`;

export const HolderItem = styled.div`
  cursor: pointer;
  margin-right: ${rem(15)};

  &:last-child {
    margin-right: 0;
  }

  ${mediaQuery.laptop`
    display: flex;
    margin: 0 ${rem(24)};
    position: relative;

    &:after {
      bottom: 0;
      background-color: ${colors.greyDark};
      content: '';
      height: ${rem(10)};
      margin: auto;
      position: absolute;
      right: ${rem(-24)};
      top: 0;
      width: ${rem(1)};
    }

    &:last-child {
      padding-right: 0;

      &:after {
        display: none;
      }
    }

    * {
      pointer-events: none;
    }
  `}
`;

export const Text = styled.p`
  display: none;

  ${mediaQuery.laptop`
    ${fonts.medium()}
    display: block;
    margin-right: ${rem(8)};
  `}
`;

export const Title = styled.p`
  ${fonts.medium(700)}

  ${mediaQuery.laptop`
    ${fonts.large(700)}
  `}
`;
