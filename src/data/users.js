import Profile from 'assets/images/profile-header-ph.png';

export default [
  {
    image: Profile,
    name: 'Ana Exemplo',
    email: 'ana@exemplo.com.br',
    phone: '(51) 99988-6633',
    role: 'Recepcionista',
  },
  {
    image: Profile,
    name: 'Matheus Henrique',
    email: 'matheus@exemplo.com.br',
    phone: '(11) 99988-6633',
    role: 'Oftalmologista',
  },
  {
    image: Profile,
    name: 'Guilherme Leonardo',
    email: 'guilherme@exemplo.com.br',
    phone: '(21) 99988-6633',
    role: 'Exemplo 1',
  },
  {
    image: Profile,
    name: 'Rafael Almeida',
    email: 'rafael@exemplo.com.br',
    phone: '(21) 99988-6633',
    role: 'Exemplo 2',
  },
  {
    image: Profile,
    name: 'Isabela Molinari',
    email: 'isabela@exemplo.com.br',
    phone: '(21) 99988-6633',
    role: 'Exemplo 3',
  },
  {
    image: Profile,
    name: 'Samuel Souza',
    email: 'samuel@exemplo.com.br',
    phone: '(11) 99652-3659',
    role: 'Exemplo 4',
  },
];
