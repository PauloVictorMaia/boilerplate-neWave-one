import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
var slugify = require('slugify');

import SideItem from 'commons/side-item';

import { Container, ItemHolder } from './sidebar.style';

const Sidebar = ({ activeItem, items }) => {
  const setActive = (text) => {
    return activeItem.toLowerCase() === text.toLowerCase();
  };

  return (
    <Container>
      {items.map(({ text, icon }) => {
        return (
          <ItemHolder key={text}>
            <Link to={`/${slugify(text, { lower: true })}`}>
              <SideItem active={setActive(text)} icon={icon} text={text} />
            </Link>
          </ItemHolder>
        );
      })}
    </Container>
  );
};

Sidebar.propTypes = {
  activeItem: PropTypes.string.isRequired,
  items: PropTypes.array.isRequired,
};

export default Sidebar;
