import React from 'react';
import { Form } from './form-container.styles';
import PropTypes from 'prop-types';

const FormContainer = ({ children }) => {
    return (
        <Form>
            {children}
        </Form>
    )
}

FormContainer.propTypes = {
  children: PropTypes.any,
};

export default FormContainer;
