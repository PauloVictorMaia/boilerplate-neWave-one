import React from 'react';
import { AppointmentsContainer, FormAppointment, InputContainer, ConfirmButton, CancelButton, TitleForm, ContentSelects, ButtonsContainer } from './create-payment.style';
import Select from 'commons/select';

const CreatePayment = () => {
  return (
    <AppointmentsContainer>
      <TitleForm>Dados do novo método</TitleForm>
      <FormAppointment>
        <InputContainer>
          <Select label='Forma de pagamento'></Select>
        </InputContainer>
        <ContentSelects>
          <InputContainer>
            <Select label='Operadora'></Select>
          </InputContainer>
          <InputContainer>
            <Select label='Parcelamento'></Select>
          </InputContainer>
          <ButtonsContainer>
            <ConfirmButton>Salvar</ConfirmButton>
            <CancelButton>Cancelar</CancelButton>
          </ButtonsContainer>
        </ContentSelects>
      </FormAppointment>
    </AppointmentsContainer>
  );
};

export default CreatePayment;
