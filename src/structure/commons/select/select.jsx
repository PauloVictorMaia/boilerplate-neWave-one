import React from 'react';
import {
  LabelForm,
  SelectContainer,
  SelectForm,
  SelectOption,
} from './select.style';
import PropTypes from 'prop-types';

const Select = ({ placeholder, label, setValue, id, options }) => {
  return (
    <SelectContainer>
      <LabelForm htmlFor={id}>{label}</LabelForm>
      <SelectForm
        id={id}
        name={id}
        placeholder={placeholder}
        onChange={({ target }) => setValue(target.value)}
      >
        <SelectOption hidden value>
          Selecione
        </SelectOption>
        {options &&
          options.map((option) => (
            <SelectOption key={option}>{option}</SelectOption>
          ))}
      </SelectForm>
    </SelectContainer>
  );
};

Select.propTypes = {
  label: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  setValue: PropTypes.any,
  options: PropTypes.array,
  placeholder: PropTypes.string,
};

export default Select;
