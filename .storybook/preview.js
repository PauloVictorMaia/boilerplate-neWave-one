import { addParameters } from '@storybook/react';
import { themes } from '@storybook/theming';

import customViewports from './config/viewports';

addParameters({
  viewport: { viewports: customViewports },
  options: {
    //theme: themes.dark,
    panelPosition: 'right',
  }
});
