import React from 'react';
import Sidebar from '.';
import GlobalStyles from 'styles/globalStyles';
import { withKnobs } from '@storybook/addon-knobs';
import data from './data'

export default {
  title: 'Sidebar',
  decorators: [withKnobs],
  parameters: {
    backgrounds: [{ name: 'black', value: '#000' }, { name: 'white', value: '#fff' }],
  },
};

export const Default = () =>(
  <>
    <GlobalStyles />
    <Sidebar activeItem={'dashboard'} items={data} />
  </>
);
