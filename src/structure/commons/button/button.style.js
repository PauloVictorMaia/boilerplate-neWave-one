import styled, { css } from 'styled-components';
import { rem, colors } from 'styles/variables';
import fonts from 'styles/fonts';

const setModifier = (modifier) => {
  switch (modifier) {
    case 'secondary':
      return `
        background-color: ${colors.transparent};
        border: ${rem(1)} solid ${colors.grey};
        color: ${colors.grey};
      `;
  }
};

export const ButtonWrapper = styled.button`
  ${fonts.small()}
  background-color: ${({ bgColor }) =>
    bgColor ? colors[bgColor] : colors.spanColor};
  border-radius: ${rem(4)};
  color: ${colors.white};
  cursor: pointer;
  height: ${rem(35)};
  padding: 0 ${rem(20)};
  width: ${rem(115)};

  ${({ modifier }) =>
    modifier &&
    css`
      ${setModifier(modifier)}
    `}
`;
