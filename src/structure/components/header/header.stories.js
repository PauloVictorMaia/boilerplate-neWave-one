import React from 'react';
import Header from '.';
import GlobalStyles from 'styles/globalStyles';
import { withKnobs } from '@storybook/addon-knobs';

export default {
  title: 'Header',
  decorators: [withKnobs],
  parameters: {
    backgrounds: [{ name: 'black', value: '#000' }, { name: 'white', value: '#fff' }],
  },
};

export const Default = () =>(
  <>
    <GlobalStyles />
    <Header />
  </>
);
