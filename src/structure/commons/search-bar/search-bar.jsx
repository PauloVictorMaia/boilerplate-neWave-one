import React from 'react';
import PropTypes from 'prop-types';

import { LoupeIcon, SearchContainer, SearchInput } from './search-bar.style';

const SearchBar = ({ placeholder }) => {
  return (
    <SearchContainer>
      <SearchInput placeholder={placeholder} type="text"/>
      <LoupeIcon />
    </SearchContainer>
  );
};

SearchBar.propTypes = {
  placeholder: PropTypes.string,
};

SearchBar.defaultProps = {
  placeholder: '',
};

export default SearchBar;
