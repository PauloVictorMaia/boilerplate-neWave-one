const { resolve } = require('path');
const webpack = require('webpack');

const commonConfig = require('./config.json');
const aliasConfig = require('./alias');

module.exports = {
  devServer: {
    hot: true,
    host: '0.0.0.0',
    port: 5050,
    publicPath: '/',
    historyApiFallback: true,
    disableHostCheck: true,
    stats: 'errors-only',
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
    open: false,
  },
  output: {
    filename: 'bundle_[hash].js',
    path: resolve(__dirname, '../public'),
    publicPath: '/',
  },
  performance: { hints: false },
  context: resolve(__dirname, '../src'),
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
          },
        },
      },
      {
        test: /\.(png|jpe?g|gif|ico|mp4|mov|svg|webm|pdf|zip)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name].[ext]',
              useRelativePath: false,
              outputPath: 'assets',
              context: 'assets',
            },
          },
        ],
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name].[ext]',
              outputPath: 'assets',
              context: 'assets',
            },
          },
        ],
      },
    ],
  },
  resolve: {
    extensions: [
      '.js',
      '.jsx',
      '.json',
      '.css',
      '.svg',
      '.pdf',
      '.zip',
      'mp4',
      'jpg',
      'png',
    ],
    alias: aliasConfig,
  },
  plugins: [
    new webpack.DefinePlugin({
      env: JSON.stringify(process.env.NODE_ENV),
      config: JSON.stringify(require(`./config.json`)),
      common: JSON.stringify(commonConfig),
      ns: JSON.stringify(commonConfig.namespace),
    }),
  ],
};
