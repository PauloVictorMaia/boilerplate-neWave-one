import React from 'react';
import IconContainer from '.';
import { withKnobs } from '@storybook/addon-knobs';

import DashboardIcon from 'icons/Dashboard'

export default {
  title: 'IconContainer',
  decorators: [withKnobs],
  parameters: {
    backgrounds: [{ name: 'black', value: '#000' }, { name: 'white', value: '#fff' }],
  },
};

export const Default = () => <IconContainer color={'black'} height={20} icon={DashboardIcon} width={20} />;
