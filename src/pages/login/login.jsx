import React from 'react';
import { LoginContainer, LogoImageContainer, LogoArea } from './login.styles';
import LoginForm from 'components/login-form';

function Login() {
  return (
    <LoginContainer>
      <LogoImageContainer>
        <LogoArea />
      </LogoImageContainer>
      <LoginForm />
    </LoginContainer>
  );
}

export default Login;
