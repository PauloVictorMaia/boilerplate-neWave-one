import React from 'react';
import PropTypes from 'prop-types';

const AsyncRoute = ({ component }) => {
  const AsyncComponent = require(`pages/${component}.jsx`).default;

  return <AsyncComponent />;
};

AsyncRoute.propTypes = {
  component: PropTypes.string.isRequired,
};

export default AsyncRoute;
