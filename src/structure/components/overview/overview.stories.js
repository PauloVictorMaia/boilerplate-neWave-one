import React from 'react';
import Overview from '.';
import GlobalStyles from 'styles/globalStyles';
import { withKnobs } from '@storybook/addon-knobs';

export default {
  title: 'Overview',
  decorators: [withKnobs],
  parameters: {
    backgrounds: [{ name: 'black', value: '#000' }, { name: 'white', value: '#fff' }],
  },
};

export const Default = () =>(
  <>
    <GlobalStyles />
    <Overview download={{ text: 'exportar relatório' }} />
  </>
);
