import React from 'react';
import PropTypes from 'prop-types';
import GlobalStyles from 'styles/globalStyles';

import SideItems from '../../components/sidebar/data';

import Header from 'components/header';
import SideBar from 'components/sidebar';
import Overview from 'components/overview';
import BottomNavigation from 'commons/bottom-navigation';

import { Container, Content, Main } from './layout.style';

const Layout = ({
  titleOverview,
  children,
  addOverview,
  filterOverview,
  downloadOverview,
  activeItem,
}) => {
  return (
    <Container>
      <GlobalStyles />
      <Header />
      <Main>
        <SideBar items={SideItems} activeItem={activeItem} />
        <Content>
          <Overview
            title={titleOverview}
            filter={filterOverview}
            add={addOverview}
            download={downloadOverview && { text: 'exportar relatório' }}
          />
          {children}
        </Content>
      </Main>
      <BottomNavigation />
    </Container>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired,
  addOverview: PropTypes.object,
  filterOverview: PropTypes.object,
  downloadOverview: PropTypes.object,
  activeItem: PropTypes.string,
  titleOverview: PropTypes.string.isRequired,
};

export default Layout;
