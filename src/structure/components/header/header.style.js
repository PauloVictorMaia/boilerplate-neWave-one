import styled from 'styled-components';

import { rem, mediaQuery } from 'styles/variables';
import fonts from 'styles/fonts';

import CircleArrow from 'icons/CircleArrow';
import Config from 'icons/Config';
import Hamburguer from 'icons/Hamburguer';
import Notification from 'icons/Notification';

export const IconArrow = styled(CircleArrow)`
  display: none;
  height: ${rem(16)};
  margin-left: ${rem(15)};
  width: ${rem(16)};
  
  ${mediaQuery.laptop`
    display: block;
  `}
`;

export const IconConfig = styled(Config)`
  display: none;
  height: ${rem(20)};
  margin-right: ${rem(16)};
  width: ${rem(20)};
  
  ${mediaQuery.laptop`
    display: block;
  `}
`;

export const IconHamburguer = styled(Hamburguer)`
  height: ${rem(16)};
  margin-right: ${rem(16)};
  width: ${rem(22)};
  
  ${mediaQuery.laptop`
    display: none;
  `}
`;

export const IconNotification = styled(Notification)`
  display: none;
  height: ${rem(20)};
  margin-right: ${rem(19)};
  width: ${rem(20)};
  
  ${mediaQuery.laptop`
    display: block;
  `}
`;

export const HeaderContainer = styled.div`
  align-items: center;
  display: flex;
  height: ${rem(80)};
  justify-content: space-between;
  padding: ${rem(16)};
  width: 100%;

  ${mediaQuery.laptop`
    justify-content: flex-start;
    padding: 0 ${rem(18)} 0 ${rem(33)};
  `}
`;

export const HeaderLogo = styled.picture`  
  ${mediaQuery.laptop`
    margin-right: ${rem(65)};
  `}
`;

export const ProfileContainer = styled.div`
  display: none;

  ${mediaQuery.laptop`
    display: flex;
  `}
`;

export const ProfileImage = styled.img`
  margin-right: ${rem(10)};
`;

export const ProfileName = styled.p`
  ${fonts.small(700)}
`;

export const ProfilePosition = styled.p`
  ${fonts.small()}
`;

export const ProfileText = styled.div`
  display: flex;
  flex-direction: column;
`;

export const SearchContainer = styled.div`
  display: none;
  flex-grow: 1;

  ${mediaQuery.laptop`
    display: block;
  `}
`;
