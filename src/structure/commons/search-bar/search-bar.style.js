import styled from 'styled-components';

import { rem, colors } from 'styles/variables';
import fonts from 'styles/fonts';

import Loupe from 'icons/Loupe';

export const LoupeIcon = styled(Loupe)`
  height: ${rem(20)};
  width: ${rem(20)};
`;

export const SearchContainer = styled.div`
  align-items: center;
  border: ${rem(1)} solid ${colors.grey};
  border-radius: ${rem(60)};
  display: flex;
  height: ${rem(35)};
  padding-left: ${rem(13)};
  padding-right: ${rem(17)};
  width: ${rem(315)};
`;

export const SearchInput = styled.input`
  ${fonts.medium()}
  border: none;
  flex-grow: 1;

  &::placeholder {
    color: ${colors.grey};
  }
`;
