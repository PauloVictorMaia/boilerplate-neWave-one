import React from 'react';
import styled from 'styled-components';
import { colors, rem } from 'styles/variables';
import { IMaskMixin } from 'react-imask';

export const LabelForm = styled.label`
  font-size: ${rem(13)};
  margin-bottom: ${rem(5)};
  color: ${colors.grey};
  padding: ${rem(3)};
`;

export const InputContainer = styled.div`
  width: 100%;
`;

export const InputForm = styled.input`
  border: ${rem(1)} solid ${colors.grey};
  border-radius: ${rem(4)};
  display: block;
  height: ${rem(35)};
  margin-bottom: ${rem(15)};
  padding-left: ${rem(11)};
  width: 100%;
`;

export const MaskedInput = IMaskMixin(({ inputRef, ...props }) => (
  <InputForm {...props} ref={inputRef} />
));
