import DashboardIcon from 'icons/Dashboard'
import DoctorIcon from 'icons/Doctor'
import PharmacistIcon from 'icons/Pharmacist'
import ExamIcon from 'icons/Exam'
import PharmacyIcon from 'icons/Pharmacy'
import SilhouetteIcon from 'icons/Silhouette'
import PinIcon from 'icons/Pin'
import CreditCardIcon from 'icons/CreditCard'
import MegaphoneIcon from 'icons/Megaphone'

export default [
  {
    icon: DashboardIcon,
    text: 'Dashboard',
  },
];