import styled from 'styled-components';
import { rem, colors } from 'styles/variables';

export const InfoCardContainer = styled.div`
  background-color: ${colors.primaryColor};
  border-radius: ${rem(5)};
  cursor: pointer;
  display: flex;
  flex-direction: column;
  height: ${rem(160)};
  padding: ${rem(10)};
  width: 100%;
`;

export const ImageContainer = styled.div`
  align-items: center;
  background-color: ${colors.secondaryColor};
  border-radius: 50%;
  display: flex;
  height: ${rem(70)};
  justify-content: center;
  width: ${rem(70)};
`;

export const TitleCard = styled.span`
  font-size: ${rem(15)};
  font-weight: bold;
  margin-bottom: ${rem(5)};
  margin-top: ${rem(5)};
`;

export const SpanContent = styled.span`
  color: ${colors.spanColor};
  font-size: ${rem(12)};
`;
