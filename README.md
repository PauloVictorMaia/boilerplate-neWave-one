# React Boilerplate

Estrutura de projetos front-end com o seguinte stack

- React
  - React Router
  - Classnames
  - PropTypes
  - SVGR
  - History
- SCSS
- Pre-commit
  - ESLint
  - Stylelint
  - Prettier

# Instalação

`| npm i |`

# Setup de sua IDE

Para manter a qualidade de código e ter sempre um padrão entre todos da equipe o projeto tem regras definidas para javascript e css/scss.

Usamos o Eslint/Prettier para Javascript/React e o Stylelint para SCSS.

É necessário a integração dessas regras com o sua IDE favorita. Recomendamos o uso do Visual Studio Code com os seguintes plugins:

| Plugin | README |

| --------- | ------------------------------------------------------------------------------------------ |

`| ESlint |` [check plugin](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) |

`| Prettier |` [check plugin](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) |

`| stylelint |` [check plugin](https://marketplace.visualstudio.com/items?itemName=shinnn.stylelint) |

# Pre-commit

Antes de commitar as alterações, o Pre-commit vai rodar as validações do ESLint e Stylelint. Caso haja algum erro, não será possível commitar.

# Executando o projeto

Abaixo os comandos que são usados no projeto:

| Comando | Description |

| ----------------------- | --------------------------------------------------------------------------------------------- |

`| npm i |` Instala as dependências do projeto |

`| npm run dev |` Executa o projeto em modo de desenvolvimento na porta `8080` |

`| npm run build |` Executa o build do projeto e gera o diretório `dist/` |

`| npm run start |` Gera uma compilação da aplicação no diretório `dist/` e roda o servidor de express na porta `3000` |

`| npm run svg-to-react |` Converte os vetores (SVGs) da pasta `assets/svg/` para componentes React |

`| npm run storybook |` Executa o Storybook com a documentação dos componentes. (Porta escolhida automaticamente) |

`| npm run build-storybook |` Gera o diretório `dist-storybook/` com o Storybook optimizado para produção |

# Padrões de código/desenvolvimento

- Temos duas branches default no projeto onde temos CI:

  - `master`: Vinculada com o ambiente de produção
  - `develop`: Vinculada com o ambiente de homologação
  - Quando em qualquer uma dessas branches ocorrer um merge, um deploy automático do projeto e do storybook será feito nos buckets configurados nas variáveis do repositório.
  - Só é permitido merge nessas branches com Pull Request.

---

- Para nomenclatura de branches, utilizamos o padrão do Gitflow

  - `feature/${nomeDaFeature}` - Para features novas
  - `hotfix/${nomeDoHotfix}` - Para hotfixes

---

- Dentro da pasta structure temos 3 pastas dividindo os componentes, onde seguimos o modelo de `Atom Design`:

  - `commons`: Átomos
  - `components`: Módulos
  - `containers`: Organismos

---

- Cada componente deve ter seu Storybook, SCSS e JSX no seu nome, com um `index.js` exportando o próprio componente como default.
- Todos os componentes precisam estar documentados no Storybook com todas as suas variações.
- Caso haja a necessidade de utilizar SVGs no projeto, jogue-os dentro da pasta `assets/svg`. Vão ser gerados componentes React (quando o comando `npm run dev` é rodado) para cada um dos ícones na pasta `src/structure/icons`.
- As rotas estão organizadas em `src/constants/routes`, onde precisa ser passado o diretório da página que for criar (`dentro de src/pages`).
- Estamos utilizando a metologia BEM para SCSS.
- O core do SCSS está dentro de `src/styles/core` e é ali onde as variáveis do projeto podem ser definidas, como cores, media queries, etc. Usamos variáveis/mixins para Cores, Fontes e MediaQueries.
- O projeto precisa ser feito em `rem` ao invés de `px`. Para facilitar, temos uma função no SCSS onde pode ser passado um valor em `px` e já faz a conversão para `rem`. Ex: `margin-bottom: rem(80px);`.

---

- No arquivo `config/config.json` você pode editar infos úteis do projeto, como: título, namespace, metatags, etc.
