import { css } from 'styled-components';

const breakpoints = {
  largeMobile: '480px',
  tablet: '768px',
  laptop: '1024px',
  desktop: '1280px',
  wide: '1440px',
};

export const colors = {
  white: '#fff',
  black: '#000',
  red: '#00ffff',
  blue: 'blue',
  grey: '#C4C4C4',
  greyLight: '#d3d3d3',
  greyDark: '#E5E5E5',
  primaryColor: '#fff',
  secondaryColor: '#F1F1F1',
  textColor: '#000',
  spanColor: '#505050',
  transparent: 'transparent',
};

export const rem = (pixels) => `${pixels / 10}rem`;

export const mediaQuery = Object.keys(breakpoints).reduce(
  (accumulator, label) => {
    accumulator[label] = (...args) => css`
      @media (min-width: ${breakpoints[label]}) {
        ${css(...args)};
      }
    `;
    return accumulator;
  },
  {},
);
