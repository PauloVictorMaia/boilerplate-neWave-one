import styled from 'styled-components';

import { rem } from 'styles/variables';

export const Container = styled.div`
  height: ${({ height }) => rem(height)};
  width: ${({ width }) => rem(width)};

  svg {
    height: 100%;
    width: 100%;

    * {
      fill: ${({ color }) => color};
    }
  }
`;
