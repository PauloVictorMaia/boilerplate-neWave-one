import styled from 'styled-components';
import { rem, mediaQuery, colors } from 'styles/variables';
import fonts from 'styles/fonts';

export const TitleSelector = styled.h4`
  ${fonts.large(700)}
  margin-left: ${rem(5)};
  margin-bottom: ${rem(21)};

  ${mediaQuery.laptop`
    margin-bottom: ${rem(24)};
 `}
`;

export const SelectorContainer = styled.div`
  border: ${rem(1)} solid ${colors.greyLight};
  border-radius: ${rem(10)};
  display: flex;
  flex-direction: column;
  max-height: ${rem(220)};
  width: 100%;
`;

export const SelectorContent = styled.div`
  display: flex;
  flex-flow: row wrap;
  height: 100%;
  padding-left: ${rem(10)};
  overflow-y: scroll;
  width: 100%;

  &::-webkit-scrollbar {
    -webkit-appearance: none;
    border-radius: ${rem(10)};
    width: ${rem(13)};
  }

  &::-webkit-scrollbar-track {
    background-color: ${colors.greyDark};
    border-radius: ${rem(3)} ${rem(8)} ${rem(8)} ${rem(3)};
    border-right: ${rem(7)} solid ${colors.white};
    width: ${rem(6)};
  }

  &::-webkit-scrollbar-thumb {
    background-color: ${colors.spanColor};
    border-radius: ${rem(3)} ${rem(8)} ${rem(8)} ${rem(3)};
    border-right: ${rem(7)} solid ${colors.white};
    width: ${rem(6)};
  }
`;

export const SelectorTop = styled.div`
  height: ${rem(13)};
`;

export const SelectorBottom = styled.div`
  height: ${rem(21)};
`;
