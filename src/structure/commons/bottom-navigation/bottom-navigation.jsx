import React from 'react';
import {
  BottomNavigationContainer,
  NavigationContainer,
  ListContainer,
} from './bottom-navigation.style';
import NotificationIcon from 'icons/Notification';
import LoupeIcon from 'icons/Loupe';
import UserIcon from 'icons/User';

import IconContainer from 'commons/icon-container';

const BottomNavigation = () => {
  return (
    <BottomNavigationContainer>
      <NavigationContainer>
        <ListContainer>
          <IconContainer height={26} icon={NotificationIcon} width={26} />
          <IconContainer height={26} icon={LoupeIcon} width={26} />
          <IconContainer height={26} icon={UserIcon} width={26} />
        </ListContainer>
      </NavigationContainer>
    </BottomNavigationContainer>
  );
};

export default BottomNavigation;
