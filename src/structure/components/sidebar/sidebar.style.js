import styled from 'styled-components';

import { colors, rem, mediaQuery } from 'styles/variables';

export const Container = styled.div`
  display: none;

  ${mediaQuery.laptop`
    background-color: ${colors.white};
    display: flex;
    flex-direction: column;
    height: 100%;
    min-width: ${rem(190)};
    padding: ${rem(24)} ${rem(18)};
  `}
`;

export const ItemHolder = styled.div`
  margin-bottom: ${rem(32)};
`;
